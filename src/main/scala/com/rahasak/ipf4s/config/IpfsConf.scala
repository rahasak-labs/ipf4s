package com.rahasak.ipf4s.config

import com.typesafe.config.ConfigFactory

trait IpfsConf {

  // config object
  val ipfsConf = ConfigFactory.load("ipfs.conf")

  // ipfs config
  lazy val ipfsHost = ipfsConf.getString("ipfs.host")
  lazy val ipfsPort = ipfsConf.getInt("ipfs.port")
  lazy val ipfsDir = ipfsConf.getInt("ipfs.dir")

  // ipfs cluster config
  lazy val ipfsClusterHost = ipfsConf.getString("ipfs-cluster.host")
  lazy val ipfsClusterPort = ipfsConf.getInt("ipfs-cluster.port")

}
