package com.rahasak.ipf4s.ipfs

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.scaladsl.Source
import akka.stream.{ActorMaterializer, ActorMaterializerSettings}
import com.rahasak.ipf4s.config.IpfsConf

import java.io.File
import scala.concurrent.Await
import scala.concurrent.duration._

object Ipf4s extends App with IpfsConf {

  // actor system rahasak
  implicit val system = ActorSystem.create("rahasak")
  implicit val materializer = ActorMaterializer(ActorMaterializerSettings(system))
  implicit val ex = system.dispatcher

  //println(id())
  //println(peers())
  //println(add(".ipfs/block0001"))
  //println(cat("QmRWT4zCTvHqQkdNJM3BGkWWT6AogCkfRKT2D9VCqWhHtq"))
  //println(pin("QmRWT4zCTvHqQkdNJM3BGkWWT6AogCkfRKT2D9VCqWhHtq"))
  println(pins())
  //println(status("QmRWT4zCTvHqQkdNJM3BGkWWT6AogCkfRKT2D9VCqWhHtq"))

  def id(): Option[String] = {
    val future = Http(system).singleRequest(
      HttpRequest(
        HttpMethods.GET,
        "http://192.168.64.48:9094/id")
    ).flatMap { response =>
      Unmarshal(response.entity).to[String]
    }.map { responseStr =>
      Option(responseStr)
    }.recover {
      case e =>
        e.printStackTrace()
        None
    }

    Await.result(future, 60.seconds)
  }

  def peers(): Option[String] = {
    val future = Http(system).singleRequest(
      HttpRequest(
        HttpMethods.GET,
        "http://192.168.64.48:9094/peers")
    ).flatMap { response =>
      Unmarshal(response.entity).to[String]
    }.map { responseStr =>
      Option(responseStr)
    }.recover {
      case e =>
        e.printStackTrace()
        None
    }

    Await.result(future, 60.seconds)
  }

  def add(filePath: String): Option[String] = {
    val streamed = Multipart.FormData(Source(
      Multipart.FormData.BodyPart.fromFile(filePath, ContentTypes.`text/plain(UTF-8)`, new File(filePath))
        :: Nil)
    )
    val future = Http(system).singleRequest(
      HttpRequest(
        HttpMethods.POST,
        "http://192.168.64.48:5001/api/v0/add",
        entity = streamed.toEntity()
      )
    ).flatMap { response =>
      Unmarshal(response.entity).to[String]
    }.map { responseStr =>
      Option(responseStr)
    }.recover {
      case e =>
        e.printStackTrace()
        None
    }

    Await.result(future, 60.seconds)
  }

  def cat(cid: String): Option[String] = {
    val api = s"http://192.168.64.48:5001/api/v0/cat?arg=$cid"
    val future = Http(system).singleRequest(
      HttpRequest(
        HttpMethods.POST,
        api)
    ).flatMap { response =>
      Unmarshal(response.entity).to[String]
    }.map { responseStr =>
      Option(responseStr)
    }.recover {
      case e =>
        e.printStackTrace()
        None
    }

    Await.result(future, 60.seconds)
  }

  def pin(cid: String): Option[String] = {
    val api = s"http://192.168.64.48:9094/pins/ipfs/$cid?mode=recursive&name=&replication-max=0&replication-min=0&shard-size=0&user-allocations="
    val future = Http(system).singleRequest(
      HttpRequest(
        HttpMethods.POST,
        api)
    ).flatMap { response =>
      Unmarshal(response.entity).to[String]
    }.map { responseStr =>
      Option(responseStr)
    }.recover {
      case e =>
        e.printStackTrace()
        None
    }

    Await.result(future, 60.seconds)
  }

  def pins(): Option[String] = {
    val future = Http(system).singleRequest(
      HttpRequest(
        HttpMethods.GET,
        "http://192.168.64.48:9094/pins")
    ).flatMap { response =>
      Unmarshal(response.entity).to[String]
    }.map { responseStr =>
      Option(responseStr)
    }.recover {
      case e =>
        e.printStackTrace()
        None
    }

    Await.result(future, 60.seconds)
  }

  def status(cid: String): Option[String] = {
    val future = Http(system).singleRequest(
      HttpRequest(
        HttpMethods.GET,
        s"http://192.168.64.48:9094/pins/$cid")
    ).flatMap { response =>
      Unmarshal(response.entity).to[String]
    }.map { responseStr =>
      Option(responseStr)
    }.recover {
      case e =>
        e.printStackTrace()
        None
    }

    Await.result(future, 60.seconds)
  }

}
